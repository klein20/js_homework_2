////////////////////Завдання 1///////////////////////////////////////

let minute = Math.floor(Math.random() * (59 + 1));

if (minute >= 0 && minute <= 14) {
    console.log("Перша чверть")
} else if (minute >= 15 && minute <= 29) {
    console.log("Друга чверть")
} else if (minute >= 30 && minute <= 44) {
    console.log("Третя чверть")
} else {
    console.log("Четверта чверть")
}


////////////////////Завдання 2///////////////////////////////////////
let lang = "en";
let rusDays = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
let engDays = ['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn']

////////////////////if///////////////////////////////////////

if (lang === "en") {
    console.log(engDays)
} else {
    console.log(rusDays)
}


////////////////////switch///////////////////////////////////////
switch (lang) {
    case "ru":
        console.log(rusDays)
        break
    case "en":
        console.log(engDays)
        break
    default:
        console.log("I don't know")
}

////////////////////object///////////////////////////////////////
let language = {
    ru: ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'],
    en: ['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn']
}
console.log(language[lang]);

////////////////////Завдання 3///////////////////////////////////////


let array = []
let arrayMax = 10
let product = 1;
for (let i = 0; i < arrayMax; i++)
    array.push(Math.floor(Math.random() * 10 + 1))

for (let i = 0; i < array.length; i++) {
    product *= array[i]
}

console.log(array, product)




